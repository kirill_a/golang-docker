package main

import (
    "gopkg.in/gographics/imagick.v2/imagick"
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
	"os"
)


var _go_basePath string = "/go/"
var _fileCacheDir string = "/cache/"
var _maxImageWidth uint = 400
//var mw *imagick.MagickWand = nil


func urlEncode(str string) (string) {
    return url.QueryEscape(str)
}


func handler(w http.ResponseWriter, r *http.Request) {
	var err error

	if r.URL.Path[1:] != "" {
		switch {
		case r.URL.Path[1:] == "small":

			fmt.Println("=========================== ", r.URL)

			width := r.URL.Query().Get("w")
			imgurl := r.URL.Query().Get("url")
			ww, _ := strconv.ParseInt(width, 10, 32)

			if len(imgurl) == 0 {
				http.Error(w, "image not set", 500)
				return
			}
			
			start := time.Now()
			
			fileName := _fileCacheDir + urlEncode(imgurl)
			
			var imgData []byte = nil
			if _, err := os.Stat(fileName); os.IsNotExist(err) {
				fmt.Println("file ", fileName, " doesn't exists in cache")
				imgData, err = downloadImage(imgurl)
				if err != nil {
					http.Error(w, err.Error(), 500)
					return
				}
				fmt.Println("downloadImage duration is %s", time.Since(start))

				// path/to/whatever does not exist
				start := time.Now()
				imgData, err = resizeMagickWand(imgData, _maxImageWidth)
				fmt.Println("_default resizeMagickWand duration is %s", time.Since(start))
				
				err = ioutil.WriteFile(fileName, imgData, 0777)
				if err != nil {
					fmt.Println("problem while saving new file ", fileName)
				}
			} else{
				imgData, _ = ioutil.ReadFile(fileName)
			}
			

			if ww > 0 {
				start := time.Now()
				imgData, err = resizeMagickWand(imgData, uint(ww))
				fmt.Println("resizeMagickWand duration is %s", time.Since(start))
				
				if err != nil {
					http.Error(w, err.Error(), 500)
					return
				}
			}

			//fmt.Fprintf(w, "zzzzzzzzzz  %s", imgData)
			w.Write(imgData)

//		case r.URL.Path[1:] == "big":
//			f, _ := ioutil.ReadFile("/home/arbuzov/maxresdefault.jpg")
//			fmt.Fprintln(w, "%s", f)
		}
	} else {
        text, err := ioutil.ReadFile(_go_basePath + "index.html")
		if err == nil {
			fmt.Fprintln(w, string(text))
		} else {
			fmt.Fprintln(w, _go_basePath + "index.html is n't found")
        }

	}
}

func main() {

    if _, err := os.Stat(_fileCacheDir); os.IsNotExist(err) {
        fmt.Println("cache folder '", _fileCacheDir, "' does not exists")
    }
    
    if _, err := os.Stat(_go_basePath); os.IsNotExist(err) {
        fmt.Println("go folder '", _go_basePath, "' does not exists")
    }

    fmt.Printf("Server started.\n")
	http.HandleFunc("/", handler)
	http.ListenAndServe(":80", nil)
}

//func init() {
//	imagick.Initialize()
//	// TODO send imagick.Terminate to main() for clean deconstruction
//}

func getProportionValue(oldValue uint, proportion float32) uint {
	return uint(float32(oldValue) * proportion)
}

func downloadImage(urlStr string) ([]byte, error) {

	urlData, err := url.Parse(urlStr)
	if err != nil {
		return nil, errors.New("error when parsing url " + urlStr)
	}

	req, err := http.NewRequest("GET", urlData.Scheme+"://"+urlData.Host+"/"+urlData.Path, bytes.NewBufferString(urlData.RawQuery))
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10")
	req.Header.Set("Referer", urlData.Scheme+"://"+urlData.Host+"/")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func resizeMagickWand(fromImageData []byte, maxWidth uint) ([]byte, error) {
	var err error

	//if (mw == nil){
	mw := imagick.NewMagickWand()
	//}
	defer mw.Destroy()

	err = mw.ReadImageBlob(fromImageData)
	if err != nil {
		return nil, err
	}
	//	fmt.Println(" ================================ ")

	filter := imagick.FILTER_BOX
	w := mw.GetImageWidth()
	h := mw.GetImageHeight()
	var newWidth uint = 0

	if w > maxWidth {
		newWidth = maxWidth
	} else {
		fmt.Println("no need any transformation (", w, "/", maxWidth, ")")
		return fromImageData, nil
	}

	var newHeight uint = getProportionValue(h, (float32(newWidth) / float32(w)))

	fmt.Println(w, "x", h, "=>", newWidth, "x", newHeight)
	err = mw.ResizeImage(newWidth, newHeight, filter, 1)
	if err != nil {
		return nil, err
	}

	err = mw.SetImageCompressionQuality(95)
	if err != nil {
		return nil, err
	}

	imageData := mw.GetImageBlob()

	return imageData, nil

	//	err = mw.WriteImage(newName)
	//	if err != nil {
	//		fmt.Println(err)
	//		return 0, origFileStat.Size()
	//	}

	//	newFileStat, _ := os.Stat(newName)
	//	return int(newFileStat.Size()), origFileStat.Size()
}
